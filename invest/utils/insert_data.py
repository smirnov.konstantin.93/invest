import os
import csv
from datetime import datetime
from pytz import timezone

from api.models import Portfolio, InstrumentType, ExpectedYeild

path_to_portfolio = os.path.expanduser("~/Desktop/tinkoff/positions.csv")
tz = timezone("Europe/Moscow")


def get_generator_for_reader(file_reader, keys):
    for i in file_reader:
        yield dict(zip(tuple(keys.split(",")), tuple(i)))


def read_csv(file_name):
    first_line = read_first_line(file_name)
    with open(file_name, "r", encoding='utf-8') as file_data:
        file_reader = csv.reader(file_data)
        data_csv = get_generator_for_reader(file_reader, first_line)
        i = 0
        for data in data_csv:
            if i == 0:
                pass
            else:
                insert_data(data)
            i += 1
        insert_ended_data()


def get_intrument_type_obj(istrument):
    if istrument == "Stock":
        instrument_type = InstrumentType.objects.get(pk=1)
    elif istrument == "Etf":
        instrument_type = InstrumentType.objects.get(pk=2)
    else:
        instrument_type = InstrumentType.objects.get(pk=3)

    return instrument_type


def insert_data(data):
    try:
        expected_yield = ExpectedYeild(
            figi=data.get("figi"),
            value=data.get("expected_yeild"),
            created_at=tz.localize(datetime.now())
        )
        expected_yield.save()
    except Exception:
        print("Error")

    try:
        cur_portfolio = Portfolio.objects.get(figi=data.get("figi"))

        if cur_portfolio.balance != data.get("balance") and cur_portfolio.averange_position_price != data.get("average_position_price"):
            cur_portfolio.balance = data.get("balance")
            cur_portfolio.averange_position_price = data.get("average_position_price")
            cur_portfolio.save()

    except Exception:
        portfolio = Portfolio(
            figi=data.get("figi"),
            ticker=data.get("ticker"),
            name=data.get("name"),
            currency=data.get("currency"),
            instrument_type=get_intrument_type_obj(data.get("instrument_type")),
            balance=data.get("balance"),
            averange_position_price=data.get("average_position_price"),
        )
        portfolio.save()
    else:
        cur_portfolio.save()


def insert_ended_data():
    for portfolio in Portfolio.objects.all():
        exp = ExpectedYeild.objects.filter(figi=portfolio.figi)
        portfolio = Portfolio.objects.get(figi=portfolio.figi)
        portfolio.expected_yeild.add(*exp)


def read_first_line(file_name):
    try:
        with open(file_name, "r", encoding='utf-8') as file:
            return file.readline().strip()
    except KeyError:
        print("Error")


read_csv(path_to_portfolio)
