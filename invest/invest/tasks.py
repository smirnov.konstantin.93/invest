from celery import shared_task
from django.core.mail import send_mail

from .settings import EMAIL_HOST_USER


@shared_task
def send_email(subject, html_message, recipient_list):
    send_mail(
        subject=subject,
        message=None,
        html_message=html_message,
        from_email=EMAIL_HOST_USER,
        recipient_list=recipient_list,
    )


@shared_task
def test(test):
    print(f"{test} succeed!")
