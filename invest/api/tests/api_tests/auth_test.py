import pytest
from django.utils.crypto import get_random_string
from rest_framework import status

from api.tests.factory_utils import UserFactory

incorrect_passwords = ["1234567", "123456789", "qwertyuiop", "same_as_email"]


@pytest.mark.django_db
def test_auth_sign_up_success(api_client):
    user_sign_up_data = {
        "first_name": get_random_string(),
        "last_name": get_random_string(),
        "email": get_random_string() + "@example.com",
        "password": get_random_string(),
    }
    r = api_client.post(path="/api/auth/sign_up/", data=user_sign_up_data, format="json")
    assert r.status_code == 200

    r_data = r.json()

    for user_sign_up_key, user_sign_up_value in user_sign_up_data.items():
        assert r_data[user_sign_up_key] == user_sign_up_value

    api_client.credentials(HTTP_AUTHORIZATION="Token " + r_data["token"])
    r = api_client.get(path="/api/user/")

    assert r.status_code == 200
    assert r.json()["email"] == user_sign_up_data["email"]


@pytest.mark.parametrize("password", incorrect_passwords)
@pytest.mark.django_db
def test_auth_sign_up_fail(password, api_client):
    email = get_random_string() + "@example.com"
    user_sign_up_data = {
        "first_name": get_random_string(),
        "last_name": get_random_string(),
        "email": email,
        "password": email if password == "same_as_email" else password,
    }

    r = api_client.post(path="/api/auth/sign_up/", data=user_sign_up_data, format="json")
    assert r.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_auth_sign_in_success(api_client):
    user = UserFactory.create()
    password = get_random_string()
    user.set_password(password)
    user.save()

    r = api_client.post(path="/api/auth/sign_in/", data={"email": user.email, "password": password}, format="json")

    assert r.status_code == status.HTTP_200_OK
    r_data = r.json()
    assert r_data["email"] == user.email
    assert r_data["token"]

    api_client.credentials(HTTP_AUTHORIZATION="Token " + r_data["token"])
    r = api_client.get(path="/api/user/")
    a = r.json()

    assert r.status_code == status.HTTP_200_OK
    assert r.json()["email"] == user.email


@pytest.mark.django_db
def test_auth_sign_in_fail(api_client):
    user = UserFactory.create()
    incorrect_password = get_random_string()
    r = api_client.post(
        path="/api/auth/sign_in/", data={"email": user.email, "password": incorrect_password}, format="json"
    )
    assert r.status_code == status.HTTP_400_BAD_REQUEST

    incorrect_email = get_random_string() + "@example.com"
    r = api_client.post(
        path="/api/auth/sign_in/", data={"email": incorrect_email, "password": incorrect_password}, format="json"
    )
    assert r.status_code == status.HTTP_400_BAD_REQUEST

    r = api_client.post(path="/api/auth/sign_in/", data={"email": user.email}, format="json")
    assert r.status_code == status.HTTP_400_BAD_REQUEST

    r = api_client.post(path="/api/auth/sign_in/", data={"password": incorrect_password}, format="json")
    assert r.status_code == status.HTTP_400_BAD_REQUEST
