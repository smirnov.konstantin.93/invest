import random

import pytest
from rest_framework import status

from api.tests.factory_utils.portfolio import InstrumentTypeFactory, PortfolioFactory, ExpectedYeildFactory


@pytest.mark.django_db
def test_get_instrument_type_list_success(api_client):
    """ Успешное получение всех типов финансовых инструментов"""
    instrument_type_count = random.randint(1, 10)
    [InstrumentTypeFactory.create() for _ in range(0, instrument_type_count)]

    r = api_client.get(path="/api/portfolio/get_instrument_types/")

    assert r.status_code == status.HTTP_200_OK
    assert len(r.json()) == instrument_type_count
    for instrument_type in r.json():
        assert instrument_type["id"]
        assert instrument_type["name"]


@pytest.mark.django_db
def test_get_portfolio_all_list_success(api_client):
    """ Успешное получение всех активов """
    expl_yeild = ExpectedYeildFactory.create()
    portfolio = PortfolioFactory.create()
    portfolio.expected_yeild.add(expl_yeild)

    r = api_client.get(path="/api/portfolio/")
    r_data = r.json()[0]

    assert r_data['figi']
    assert r_data['ticker']
    assert r_data['name']
    assert r_data['currency']
    assert r_data['instrument_type']['name']
    assert r_data['balance']
    assert r_data['averange_position_price']
    assert r_data['expected_yeild'][0]['value']
    assert r_data['expected_yeild'][0]['created_at']


@pytest.mark.django_db
def test_get_portfolio_by_figi_success(api_client):
    """ Успешное получение активов по figi """
    expl_yeild = ExpectedYeildFactory.create()
    portfolio = PortfolioFactory.create()
    portfolio.expected_yeild.add(expl_yeild)

    r = api_client.get(path="/api/portfolio/?ticker="+portfolio.ticker)
    r_data = r.json()[0]

    assert r_data['figi']
    assert r_data['ticker']
    assert r_data['name']
    assert r_data['currency']
    assert r_data['instrument_type']['name']
    assert r_data['balance']
    assert r_data['averange_position_price']
    assert r_data['expected_yeild'][0]['value']
    assert r_data['expected_yeild'][0]['created_at']


@pytest.mark.django_db
def test_get_portfolio_by_figi_fail(api_client):
    """ Ошибка получения активов по ticker """
    expl_yeild = ExpectedYeildFactory.create()
    portfolio = PortfolioFactory.create()
    portfolio.expected_yeild.add(expl_yeild)

    ticker_param = 'abracadabra'
    r = api_client.get(path="/api/portfolio/?ticker="+ticker_param)

    assert r.status_code == status.HTTP_404_NOT_FOUND
