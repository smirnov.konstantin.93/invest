import random
from datetime import datetime
from pytz import timezone

import factory

from api.models import InstrumentType, ExpectedYeild, Portfolio

tz = timezone("Europe/Moscow")


class InstrumentTypeFactory(factory.django.DjangoModelFactory):
    """ Класс, который будет создавать объекты модели InstrumentType для тестов  """
    class Meta:
        model = InstrumentType

    name = "Instrument type" + str(random.randint(1, 15))


class ExpectedYeildFactory(factory.django.DjangoModelFactory):
    """ Класс, который будет создавать объекты модели ExpectedYeild для тестов """
    class Meta:
        model = ExpectedYeild

    figi = "figi" + str(random.randint(1, 20))
    value = round(random.randint(1, 1000) * random.random(), 5)
    created_at = tz.localize(datetime.now()).strftime("%Y-%m-%d %H:%M:%S")


class PortfolioFactory(factory.django.DjangoModelFactory):
    """ Класс, который будет создавать объекты модели Portfolio для тестов """
    class Meta:
        model = Portfolio

    figi = "figi" + str(random.randint(1, 20))
    ticker = "ticker" + str(random.randint(1, 20))
    name = "name" + str(random.randint(1, 20))
    currency = "currency" + str(random.randint(1, 20))
    instrument_type = factory.SubFactory(InstrumentTypeFactory)
    balance = round(random.randint(1, 1000) * random.random(), 5)
    averange_position_price = round(random.randint(1, 1000) * random.random(), 5)

