from drf_yasg.utils import no_body, swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response
from django_filters import rest_framework as filters
from django.db.models import Sum
from datetime import date
from pycbrf import ExchangeRates

from api.models import Portfolio
from api.serializers import PortfolioSerializer, TopPortfolioSerializer
from api.filters import PortfolioFilters


USD = ExchangeRates()['USD'].value
EUR = ExchangeRates()['EUR'].value


class PortfolioViewSet(GenericViewSet):
    model = Portfolio
    serializer_class = PortfolioSerializer
    queryset = Portfolio.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PortfolioFilters

    @swagger_auto_schema(
        operation_summary="get all portfolio",
        request_body=no_body,
        responses={200: PortfolioSerializer()},
    )
    def list(self, request):
        queryset = self.filter_queryset(self.get_queryset())

        return Response(self.serializer_class(queryset,
                                              many=True,
                                              context={"request": request}).data,
                        status=status.HTTP_200_OK
                        )

    @swagger_auto_schema(
        operation_summary="get top 3 actives in percents",
        request_body=no_body,
        responses={200: TopPortfolioSerializer()}
    )
    @action(methods=["GET"], detail=False)
    def get_top_actives(self, request):
        all_summa = self.queryset.aggregate(Sum('rub_summa'))
        portfolios = sorted(self.queryset,
                            key=lambda x: x.rub_summa / all_summa.get('rub_summa__sum') * 100,
                            reverse=True)
        setializer = TopPortfolioSerializer(portfolios[:3], many=True)
        return Response(setializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_summary="get all analyze actives",
        request_body=no_body,
    )
    @action(methods=["GET"], detail=False)
    def get_all_summ_actives_on_current_date(self, request):
        """ Метод возвращает
            1 - полную стоимость портфеля в рублях, на текущий момент
            2 - полную доходность потрфеля в рублях, на текущий момент
            3 - процент доходности или убытка
        """
        all_summa = self.queryset.aggregate(Sum('rub_summa'))
        current_date = date.today()

        expected_yeild_summ = []
        for i in self.queryset:
            for j in range(len(i.expected_yeild.values_list())):
                if i.expected_yeild.values_list()[j][3].date() == current_date:
                    if i.currency == 'USD':
                        expected_yeild_summ.append(i.expected_yeild.values_list()[j][2] * USD)
                    elif i.currency == 'EUR':
                        expected_yeild_summ.append(i.expected_yeild.values_list()[j][2] * EUR)
                    else:
                        expected_yeild_summ.append(i.expected_yeild.values_list()[j][2])
                j += 1

        return Response({
            'all_summa_actives': round(all_summa.get('rub_summa__sum'), 2),
            'all_expected_yeild_summa': round(sum(expected_yeild_summ), 2),
            'all_percents': round(sum(expected_yeild_summ) / all_summa.get('rub_summa__sum') * 100, 2),
        }, status=status.HTTP_200_OK)
