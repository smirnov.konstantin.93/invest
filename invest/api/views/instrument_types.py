from drf_yasg.utils import no_body, swagger_auto_schema
from rest_framework import status
from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response

from api.models import InstrumentType
from api.serializers import InstrumentTypeSerializer


class InstumentTypeViewSet(GenericViewSet):
    model = InstrumentType
    serializer_class = InstrumentTypeSerializer
    queryset = InstrumentType.objects.all()

    @swagger_auto_schema(
        operation_summary="get all instrument types",
        request_body=no_body,
        responses={200: InstrumentTypeSerializer()},
    )
    def list(self, request):
        return Response(self.serializer_class(self.queryset,
                                              many=True,
                                              context={"request": request}).data,
                        status=status.HTTP_200_OK
                        )
