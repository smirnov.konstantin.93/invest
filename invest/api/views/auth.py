from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError as DjangoValidationError
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from drf_yasg.utils import no_body, swagger_auto_schema
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from api.models.uuid_token import TokenEnum
from api.models import User, UuidToken
from api.serializers import (  # noqa
    ConfirmEmail,
    ForgotPasswordSerializer,
    SetNewPasswordSerializer,
    SignInSerializer,
    SignInSerializerResponse,
    SignUpSerializer,
    SignUpSerializerResponse,
    UserAuthOutputSerializer,
)

from api.services.email import send_email_confirmation, send_forgot_password


class AuthViewSet(GenericViewSet):
    serializer_class = UserAuthOutputSerializer
    # permission_classes_by_action = {"send_email_confirmation": [IsAuthenticated], "change_password": [IsAuthenticated]}
    #
    # def get_permissions(self):
    #     try:
    #         return [permission() for permission in self.permission_classes_by_action[self.action]]
    #     except KeyError:
    #         return super().get_permissions()

    @swagger_auto_schema(
        operation_summary="Endpoint to send confirmation email to current user",
        request_body=no_body,
        responses={200: UserAuthOutputSerializer()},
    )
    @action(methods=["POST"], detail=False)
    def send_email_confirmation(self, request):
        send_email_confirmation(request)
        return Response(self.get_serializer(request.user).data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_summary="Endpoint for confirming users email",
        request_body=ConfirmEmail,
        responses={200: ConfirmEmail()},
    )
    @action(methods=["POST"], detail=False)
    def confirm_email(self, request):
        try:
            token = UuidToken.objects.get(
                value=request.data["value"],
                expiration_date__gt=timezone.now(),
                token_type=TokenEnum.EMAIL_VERIFICATION,
            )
        except (UuidToken.DoesNotExist, DjangoValidationError):
            raise ValidationError(_("Invalid Token"))

        token.user.verify_email()
        token.delete()

        return Response(ConfirmEmail(request.data).data)

    @swagger_auto_schema(
        operation_summary="Sign up", request_body=SignUpSerializer, responses={200: SignUpSerializerResponse()},
    )
    @action(methods=["POST"], detail=False)
    def sign_up(self, request):
        if request.method == "POST":
            serializer = SignUpSerializer(data=request.data)
            if serializer.is_valid():
                user = serializer.save()
                token, _ = Token.objects.get_or_create(user=user)
                data = {
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "email": user.email,
                    "password": request.data["password"],
                    "token": token.key,
                }
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            return Response(data)

    @swagger_auto_schema(
        operation_summary="Sign in", request_body=SignInSerializer, responses={200: SignInSerializerResponse()},
    )
    @action(methods=["POST"], detail=False)
    def sign_in(self, request):
        if "email" not in request.data or "password" not in request.data:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"Error": "No email or password been given."})
        try:
            user = User.objects.get(email=request.data["email"])
        except User.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"Error": "Invalid email or password."})
        if user.check_password(request.data["password"]):
            token, _ = Token.objects.get_or_create(user=user)
            data = {"email": user.email, "token": token.key}
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"Error": "Invalid email or password."})
        return Response(data)

    @swagger_auto_schema(
        operation_summary="Endpoint for forgot user password",
        request_body=ForgotPasswordSerializer,
        responses={200: ForgotPasswordSerializer(),},
    )
    @action(methods=["POST"], detail=False)
    def forgot_password(self, request):
        try:
            user = User.objects.get(email=request.data.get("email"))
            send_forgot_password(request, user)
        except User.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        return Response(ForgotPasswordSerializer(request.data).data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_summary="Endpoint for set new password",
        request_body=SetNewPasswordSerializer,
        responses={200: UserAuthOutputSerializer(),},
    )
    @action(methods=["POST"], detail=False)
    def set_new_password(self, request):
        try:
            token = UuidToken.objects.get(
                value=request.data.get("token"),
                expiration_date__gt=timezone.now(),
                token_type=TokenEnum.FORGOT_PASSWORD,
            )
        except UuidToken.DoesNotExist:
            raise ValidationError(_("Invalid token"))

        user = token.user

        if user.email == request.data.get("password"):
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"Error": "You can't use this password"})

        try:
            validate_password(request.data.get("password"))
        except DjangoValidationError as e:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"Error": e.messages})

        token.delete()
        user.set_password(request.data.get("password"))
        user.save()
        return Response(self.serializer_class(user, context={"request": request}).data)
