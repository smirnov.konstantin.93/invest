from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError as DjangoValidationError
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from api.models import User
from api.serializers import (
    UserSerializer
)


class UserViewSet(GenericViewSet):
    model = User
    serializer_class = UserSerializer
    queryset = User.objects.all()

    @swagger_auto_schema(
        operation_summary="get current user",
        operation_id="get current user",
        responses={200: UserSerializer()}
    )
    def list(self, request, *args, **kwargs):
        return Response(self.serializer_class(self.request.user, context={"request": request}).data)
