from pytz import timezone
from drf_yasg import openapi
from drf_yasg.utils import no_body, swagger_auto_schema
from pycbrf.toolbox import ExchangeRates
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet


date_param = openapi.Parameter("date", openapi.IN_QUERY, description="date", type=openapi.FORMAT_DATE)


class CurrencyViewset(ViewSet):
    moscow_tz = timezone('Europe/Moscow')

    @swagger_auto_schema(
        operation_summary="get usd course",
        request_body=no_body,
        manual_parameters=[date_param]
    )
    @action(methods=["GET"], detail=False)
    def get_usd_course(self, request):
        """ Получаем курс доллара по ЦБРФ на заданную дату.
            Если не передавать дату, то на текущий день. """
        date = request.query_params.get("date")

        rates = ExchangeRates(date)
        return Response(data={"value": rates['USD'].value}, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_summary="get eur course",
        request_body=no_body,
        manual_parameters=[date_param]
    )
    @action(methods=["GET"], detail=False)
    def get_eur_course(self, request):
        """ Получаем курс евро по ЦБРФ на заданную дату.
            Если не передавать дату, то на текущий день. """
        date = request.query_params.get("date")

        rates = ExchangeRates(date)
        return Response(data={"value": rates['EUR'].value}, status=status.HTTP_200_OK)
