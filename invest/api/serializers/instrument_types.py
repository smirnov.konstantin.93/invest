from rest_framework import serializers

from api.models import InstrumentType


class InstrumentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = InstrumentType
        fields = ("id", "name", )
