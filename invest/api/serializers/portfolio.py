from django.db.models import Sum
from rest_framework import serializers

from api.models import Portfolio, ExpectedYeild
from .instrument_types import InstrumentTypeSerializer


class ExpectedYeildSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExpectedYeild
        fields = ("value", "created_at", )

    def to_representation(self, instance):
        return {
            'value': instance.value,
            'created_at': instance.created_at.strftime("%Y-%m-%d %H:%M:%S")
        }


class PortfolioSerializer(serializers.ModelSerializer):
    instrument_type = InstrumentTypeSerializer()
    expected_yeild = ExpectedYeildSerializer(many=True)

    class Meta:
        model = Portfolio
        fields = (
            "figi",
            "ticker",
            "name",
            "currency",
            "instrument_type",
            "balance",
            "averange_position_price",
            "expected_yeild",
            "rub_summa"
        )


class TopPortfolioSerializer(serializers.ModelSerializer):
    percents = serializers.SerializerMethodField()

    class Meta:
        model = Portfolio
        fields = (
            "figi",
            "ticker",
            "name",
            "currency",
            "instrument_type",
            "balance",
            "averange_position_price",
            "expected_yeild",
            "rub_summa",
            "percents"
        )

    def get_percents(self, obj):
        all_summa = Portfolio.objects.all().aggregate(Sum('rub_summa'))
        return round(obj.rub_summa / all_summa.get('rub_summa__sum') * 100, 2)

