from django_filters import rest_framework as filters

from api.models import Portfolio


class CharFilterInFilter(filters.BaseInFilter, filters.CharFilter):
    pass


class PortfolioFilters(filters.FilterSet):
    ticker = filters.CharFilter(field_name="ticker")

    class Meta:
        model = Portfolio
        fields = ['ticker']
