from django.template.loader import render_to_string
from django.utils.translation import gettext_lazy as _

from api.models.uuid_token import TokenEnum
from api.models import UuidToken
from invest.tasks import send_email


def send_email_confirmation(request):
    token = UuidToken.objects.create(user=request.user, token_type=TokenEnum.EMAIL_VERIFICATION)
    link = "{0}/auth/confirm_email/{1}".format(request.META.get("HTTP_ORIGIN"), token.value)
    context = {
        "title": "Email Confirmation",
        "text": "To confirm your mail, press the button",
        "button_link": link,
        "button_text": "Confirm",
    }
    html_message = render_to_string("email/base.html", context=context)
    email = {"subject": "Email Confirmation", "html_message": html_message, "recipient_list": [request.user.email]}
    send_email.delay(**email)


def send_forgot_password(request, user):
    token = UuidToken.objects.create(user=user, token_type=TokenEnum.FORGOT_PASSWORD)
    link = "{0}/auth/forgot_password/{1}".format(request.META.get("HTTP_ORIGIN"), token.value)
    context = {
        "title": _("Password Recovery"),
        "text": _("To set a new password, press the button"),
        "button_link": link,
        "button_text": _("Set new password"),
    }
    html_message = render_to_string("email/base.html", context=context)
    email = {"subject": "Forgot Password", "html_message": html_message, "recipient_list": [user.email]}
    send_email.delay(**email)
