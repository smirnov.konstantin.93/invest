from django.db import models
from pycbrf import ExchangeRates

USD = ExchangeRates()['USD'].value
EUR = ExchangeRates()['EUR'].value


class InstrumentType(models.Model):
    """ Тип акции, етф, нал """
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class ExpectedYeild(models.Model):
    figi = models.CharField(max_length=255, db_index=True)
    value = models.DecimalField(max_digits=15, decimal_places=5)
    created_at = models.DateTimeField()

    def __str__(self):
        return self.figi


class Portfolio(models.Model):
    """ Портфель """
    figi = models.CharField(max_length=255, db_index=True)
    ticker = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    currency = models.CharField(max_length=10)
    instrument_type = models.ForeignKey(InstrumentType, on_delete=models.CASCADE)
    balance = models.DecimalField(max_digits=15, decimal_places=5)
    averange_position_price = models.DecimalField(max_digits=15, decimal_places=5)
    expected_yeild = models.ManyToManyField(ExpectedYeild)
    rub_summa = models.DecimalField(max_digits=15, decimal_places=5, default=0)

    def get_currency(self):
        if self.currency == 'USD':
            return USD
        if self.currency == 'EUR':
            return EUR

    def save(self, *args, **kwargs):
        currency = self.get_currency()

        if currency:
            self.rub_summa = float(currency) * (float(self.averange_position_price) * float(self.balance))
        else:
            self.rub_summa = float(self.averange_position_price) * float(self.balance)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name
