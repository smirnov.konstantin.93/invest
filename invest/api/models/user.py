from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


class CustomUserManager(UserManager):
    def create_superuser(self, email, password=None, **extra_fields):
        super().create_superuser(username=email, email=email, password=password)


class User(AbstractUser):
    """ Модель юзера, чтобы вместо username использовать email """
    email = models.EmailField(blank=True, unique=True)
    is_email_verified = models.BooleanField(default=False)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    def save(self, *args, **kwargs):
        if not self.pk:
            self.username = self.email
        super().save(*args, **kwargs)

    def verify_email(self):
        self.is_email_verified = True
        self.save()

    def __str__(self):
        return self.email


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
