import datetime
import uuid

from django.db import models
from django.db.models import Choices

from django.utils import timezone

from invest.settings import VERIFICATION_TOKEN_EXPIRATION_TIME


class TokenEnum(Choices):
    EMAIL_VERIFICATION = "EMAIL_VERIFICATION"
    FORGOT_PASSWORD = "FORGOT_PASSWORD"


class UuidToken(models.Model):
    value = models.UUIDField()
    expiration_date = models.DateTimeField()
    user = models.ForeignKey("User", on_delete=models.CASCADE)
    token_type = models.CharField(max_length=55, choices=TokenEnum.choices, default="EMAIL_VERIFICATION")

    def save(self, *args, **kwargs):
        self.value = uuid.uuid4()
        self.expiration_date = timezone.now() + datetime.timedelta(seconds=VERIFICATION_TOKEN_EXPIRATION_TIME)
        super().save(*args, **kwargs)
