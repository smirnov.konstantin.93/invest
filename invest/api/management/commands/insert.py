from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Insert data for DB"

    def handle(self, *args, **options):
        from utils import insert_data
