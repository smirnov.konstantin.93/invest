from django.contrib import admin
from .models import User, Portfolio, InstrumentType, ExpectedYeild, UuidToken

admin.site.register(User)
admin.site.register(Portfolio)
admin.site.register(InstrumentType)
admin.site.register(ExpectedYeild)
admin.site.register(UuidToken)
