from django.conf import settings
from django.urls import path
from drf_yasg import openapi
from drf_yasg.generators import OpenAPISchemaGenerator
from drf_yasg.views import get_schema_view
from rest_framework.routers import SimpleRouter

from .views import (PortfolioViewSet,
                    CurrencyViewset,
                    UserViewSet,
                    AuthViewSet,
                    InstumentTypeViewSet
                    )


class CustomShemaGenerator(OpenAPISchemaGenerator):
    def get_shema(self, request=None, public=False):
        schema = super().get_schema(request, public)
        schema.schemes = ['HTTPS', 'HTTP']
        if settings.ENVIRONMENT == "local":
            schema.schemes.reverse()
        return schema


schema_view = get_schema_view(
    openapi.Info(
        title="Invest Application",
        default_version="0.1",
    ),
    public=True,
    generator_class=CustomShemaGenerator,
)

app_name = "api"

router = SimpleRouter()
router.register("instrument_type", InstumentTypeViewSet, basename="instrument_type")
router.register("portfolio", PortfolioViewSet, basename="portfolio")
router.register("currency", CurrencyViewset, basename="currency")
router.register("user", UserViewSet, basename="user")
router.register("auth", AuthViewSet, basename="auth")


urlpatterns = [
    path("swagger/", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger",),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc",),
]

urlpatterns += router.urls
