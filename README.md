Invest project
---
---
Installing
---
---
Build, create and start database

    docker-compose up -d --build

### Install project dependencies

    cd invest && poetry install

### Spawn a shell within the virtual environment

    poetry shell

---

### Celery and flower

### Run celery and flower

    celery -A invest flower  --address=127.0.0.1 --port=5555

___

### Run celery

    celery -A invest worker -l INFO

---


## Test

---
### Run pytest

    cd invest && pytest

### Run coverage

    coverage run --source=. -m pytest

### Show coverage report

    coverage report

___
